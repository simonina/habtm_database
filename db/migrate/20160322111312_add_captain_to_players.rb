class AddCaptainToPlayers < ActiveRecord::Migration
  def change
    add_column :players, :captain, :boolean, :default => false
  end
end
