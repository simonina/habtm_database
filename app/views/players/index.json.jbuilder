json.array!(@players) do |player|
  json.extract! player, :id, :name, :team, :email, :mobile
  json.url player_url(player, format: :json)
end
