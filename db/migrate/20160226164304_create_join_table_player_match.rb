class CreateJoinTablePlayerMatch < ActiveRecord::Migration
  def change
    create_join_table :Players, :Matches do |t|
      # t.index [:player_id, :match_id]
      # t.index [:match_id, :player_id]
    end
  end
end
