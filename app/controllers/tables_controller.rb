require 'open-uri'

class TablesController < ApplicationController
  before_action :authenticate_player!
  
  def index
    @team_names = ["Trinity 1st XI", "Trinity 2nd XI", "Trinity 3rd XI", "Trinity 4th XI"]
    @table_html = table_html_for 0 # Get first table out automatically
  end
 
  def show
    @team_names = ["Trinity 1st XI", "Trinity 2nd XI", "Trinity 3rd XI", "Trinity 4th XI"] # storing table names in array
    @table_html = table_html_for params[:id].to_i
  end
  
  private
  
    def table_html_for id
      data = Rails.cache.fetch("league_tables_#{id}", :expires_in => 7.days) do   # cache updates every 7 days
        urls = ["http://www.leinsterhockey.ie/league/104285/mens_division1_b", # each individual URL in an array
                "http://www.leinsterhockey.ie/league/102248/mens_division3",
                "http://www.leinsterhockey.ie/league/100884/mens_division6",
                "http://www.leinsterhockey.ie/league/100916/mens_division7"
              ]
        url = urls[id]
        open(url).read    # directs controller to visit each URL
      end
      page = Nokogiri::HTML(data)   # called nokogiri library
      return page.css(".leagueTable").first # finding the specific element containing leaguetable
    end
end
