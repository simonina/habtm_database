Rails.application.routes.draw do

  devise_for :players, controllers: {registrations: "registrations"}, skip: [:registrations]
  resources :news
  resources :absences
  #get "players/calendar" => "players#calendar"
  resources :players
  #get "matches/calendar" => "matches#calendar"
  resources :matches
  
  resources :calendar
  
  resources :tables
  
  resources :passwords
  
  root :to => 'calendar#index'
  
  get "/passwords" => "passwords#index"
  
end
