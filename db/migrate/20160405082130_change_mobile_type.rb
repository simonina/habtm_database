class ChangeMobileType < ActiveRecord::Migration
  def change
    change_column :players, :mobile, :string
  end
end
