class MatchesController < ApplicationController
  before_action :authenticate_player!
  before_action :insure_is_captain, except: [:index, :show]
  before_action :set_match, only: [:show, :edit, :update, :destroy]

  def index
    @matches = Match.all
  end

  def show
    
  end

  # GET /matches/new
  def new
    @match = Match.new
    date = params[:start_time]
    if date
      @date = DateTime.new date["{}(1i)"].to_i, date["{}(2i)"].to_i, date["{}(3i)"].to_i
      @match.start_time = @date
      @absent_players = Absence.where("absent_from <= ? AND absent_until >= ?", @date, @date).map(&:player)
      absent_players_ids = @absent_players.to_a.compact.map(&:id) || []
      @available_players = absent_players_ids == [] ? Player.all : Player.where("id NOT IN (?)", absent_players_ids)
      @selected_players = @match.players
      @teams = @available_players.group_by(&:team).sort
    end
  end

  # GET /matches/1/edit
  def edit
    @date = @match.start_time
    @absent_players = Absence.where("absent_from <= ? AND absent_until >= ?", @date, @date).map(&:player)
    absent_players_ids = @absent_players.map(&:id) || []
    @available_players = absent_players_ids == [] ? Player.all : Player.where("id NOT IN (?)", absent_players_ids)
    @selected_players = @match.players
    @teams = @available_players.group_by(&:team).sort
  end

  # POST /matches
  # POST /matches.json
  def create
    @match = Match.new(match_params)

    respond_to do |format|
      if @match.save
        format.html { redirect_to @match, notice: 'Match was successfully created.' }
        format.json { render :show, status: :created, location: @match }
      else
        format.html { render :new }
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @match.update(match_params)
        format.html { redirect_to @match, notice: 'Match was successfully updated.' }
        format.json { render :show, status: :ok, location: @match }
      else
        format.html { render :edit }
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @match.destroy
    respond_to do |format|
      format.html { redirect_to matches_url, notice: 'Match was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_match
      @match = Match.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def match_params
      params.require(:match).permit(:start_time, :team, :against, :venue, :competition, :score_for, :score_against, :player_ids => [])
    end
end
