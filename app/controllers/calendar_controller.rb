class CalendarController < ApplicationController
    before_action :authenticate_player!
    
    def index
        @matches = current_player.matches
    end
    
end
