# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Absence.create(reason: "FYP work", absent_from: "2016-4-3", absent_until: "2016-4-11", player_id: 1) #misses 9th
Absence.create(reason: "Geography Field Trip", absent_from: "2016-5-6", absent_until: "2016-5-8", player_id: 3) #misses 7th
Absence.create(reason: "Dislocated Shoulder", absent_from: "2016-3-20", absent_until: "2016-4-29", player_id: 20) #misses 26th, 2, 9, 16, 23
Absence.create(reason: "Twisted Ankle", absent_from: "2016-4-3", absent_until: "2016-4-18", player_id: 8) #misses 9th and 16th matches

#matches for 1's past 4 staurdays
Match.create(start_time: "2016-3-12 12:45:00", team:1, against:"Weston", venue: "Home", competition: "League", score_for: '4', score_against:  '2',)
Match.create(start_time: "2016-3-12 12:45:00", team:2, against:"Kilkenny", venue: "Home", competition: "League", score_for: '1', score_against:  '4')
Match.create(start_time: "2016-3-12 12:45:00", team:3, against:"Fingal", venue: "Away", competition: "League", score_for: '4', score_against:  '3')
Match.create(start_time: "2016-3-12 12:45:00", team:4, against:"Rathgar", venue: "Home", competition: "League", score_for: '3', score_against:  '2')

Match.create(start_time: "2016-3-19 12:45:00", team:1, against:"Kilkenny", venue: "Away", competition: "League", score_for: '2', score_against:  '3')
Match.create(start_time: "2016-3-19 12:45:00", team:2, against:"Weston", venue: "Away", competition: "League", score_for: '0', score_against:  '6')
Match.create(start_time: "2016-3-19 12:45:00", team:3, against:"Rathgar", venue: "Home", competition: "League", score_for: '2', score_against:  '4')
Match.create(start_time: "2016-3-19 12:45:00", team:4, against:"Fngal", venue: "Away", competition: "League", score_for: '2', score_against:  '2')

Match.create(start_time: "2016-3-26 12:45:00", team:1, against:"Rathgar", venue: "Home", competition: "League", score_for: '1', score_against:  '1')
Match.create(start_time: "2016-3-26 12:45:00", team:2, against:"Kilkenny", venue: "Home", competition: "League", score_for: '2', score_against:  '2')
Match.create(start_time: "2016-3-26 12:45:00", team:3, against:"Fingal", venue: "Away", competition: "League", score_for: '0', score_against:  '2')
Match.create(start_time: "2016-3-26 12:45:00", team:4, against:"Three Rock", venue: "Home", competition: "League", score_for: '1', score_against:  '0')


Match.create(start_time: "2016-4-2 12:45:00", team:1, against:"Fingal", venue: "Away", competition: "League", score_for: '3', score_against:  '2')
Match.create(start_time: "2016-4-2 12:45:00", team:2, against:"Weston", venue: "Away", competition: "League", score_for: '2', score_against:  '3')
Match.create(start_time: "2016-4-2 12:45:00", team:3, against:"Pembroke", venue: "Home", competition: "League", score_for: '2', score_against:  '1')
Match.create(start_time: "2016-4-2 12:45:00", team:4, against:"Three Rock", venue: "Away", competition: "League", score_for: '3', score_against:  '1')


#match for 1's for next 6 saturdays
Match.create(start_time: "2016-4-9 12:45:00", team:1, against:"Weston", venue: "Away", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-4-9 12:45:00", team:2, against:"Pembroke", venue: "Home", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-4-9 12:45:00", team:3, against:"Kilkenny", venue: "Home", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-4-9 12:45:00", team:4, against:"Rathgar", venue: "Away", competition: "League", score_for: '', score_against:  '')

Match.create(start_time: "2016-4-16 12:45:00", team:1, against:"Kilkenny", venue: "Home", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-4-16 12:45:00", team:2, against:"Clontarf", venue: "Away", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-4-16 12:45:00", team:3, against:"Three Rock", venue: "Away", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-4-16 12:45:00", team:4, against:"Kilkenny", venue: "Home", competition: "League", score_for: '', score_against:  '')

Match.create(start_time: "2016-4-23 12:45:00", team:1, against:"Rathgar", venue: "Away", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-4-23 12:45:00", team:2, against:"Corinthians", venue: "Home", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-4-23 12:45:00", team:3, against:"Kilkenny", venue: "Home", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-4-23 12:45:00", team:4, against:"Weston", venue: "Away", competition: "League", score_for: '', score_against:  '')

Match.create(start_time: "2016-4-30 12:45:00", team:1, against:"Fingal", venue: "Home", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-4-30 12:45:00", team:2, against:"Weston", venue: "Home", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-4-30 12:45:00", team:3, against:"Corinthians", venue: "Away", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-4-30 12:45:00", team:4, against:"Three Rock", venue: "Home", competition: "League", score_for: '', score_against:  '')

Match.create(start_time: "2016-5-7 12:45:00", team:1, against:"Three Rock", venue: "Away", competition: "Cup", score_for: '', score_against:  '')
Match.create(start_time: "2016-5-7 12:45:00", team:2, against:"Kilkenny", venue: "Home", competition: "Cup", score_for: '', score_against:  '')
Match.create(start_time: "2016-5-7 12:45:00", team:3, against:"Weston", venue: "Home", competition: "Cup", score_for: '', score_against:  '')
Match.create(start_time: "2016-5-7 12:45:00", team:4, against:"Cobh", venue: "Away", competition: "Cup", score_for: '', score_against:  '')

Match.create(start_time: "2016-5-14 12:45:00", team:1, against:"Weston", venue: "Home", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-5-14 12:45:00", team:2, against:"Pembroke", venue: "Away", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-5-14 12:45:00", team:3, against:"Fingal", venue: "Away", competition: "League", score_for: '', score_against:  '')
Match.create(start_time: "2016-5-14 12:45:00", team:4, against:"Weston", venue: "Away", competition: "League", score_for: '', score_against:  '')

#all players seeded into db - 11 per team
Player.create(
    name:"Alex Simonin", team:1, email:"as@tcd.ie", mobile:"086-1234567", captain:true, password:'12345678'
)
Player.create(
    name:"Aran Rooney", team: 1, email: "ar@tcd.ie", mobile:  "085-6556789", captain: false, password: '123456789'
)
Player.create(
    name:"Robbie Clarke", team: 1, email: "rb@tcd.ie", mobile:  "085-7856789", captain: false, password: '123456789'
)
Player.create(
    name:"John Lewis", team: 1, email: "jl@tcd.ie", mobile:  "085-0006789", captain: false, password: '123456789'
)
Player.create(
    name:"Ian Marron", team: 1, email: "im@tcd.ie", mobile:  "085-3488789", captain: false, password: '123456789'
)
Player.create(
    name:"Hugh Lavery", team: 1, email: "hl@tcd.ie", mobile:  "085-9956789", captain: false, password: '123456789'
)
Player.create(
    name:"Rory Nichols", team: 1, email: "rn@tcd.ie", mobile:  "085-2256789", captain: false, password: '123456789'
)
Player.create(
    name:"Owen Chambers", team: 1, email: "oc@tcd.ie", mobile:  "085-4456789", captain: false, password: '123456789'
)
Player.create(
    name:"Tom Gibbs", team: 1, email: "tg@tcd.ie", mobile: "086-2345678", captain: false, password: '123456789'
)
Player.create(
    name:"Cillian Hynes", team: 1, email: "ch@tcd.ie", mobile:  "085-8956789", captain: false, password: '123456789'
)
Player.create(
    name:"David Stead", team: 1, email: "ds@tcd.ie", mobile:  "086-3456789", captain: false, password: '123456789'
)
Player.create(
    name:"Matty Cockerail", team: 2, email: "mc@tcd.ie", mobile:  "086-4567890", captain: true, password: '123456789'
)
Player.create(
    name:"Rich Connon", team: 2, email: "rc@tcd.ie", mobile:  "085-3456789", captain: false, password: '123456789'
)
Player.create(
    name:"Toby shwerwood", team: 2, email: "ts@tcd.ie", mobile:  "085-3456781", captain: false, password: '123456789'
)
Player.create(
    name:"Ricky McMahon", team: 2, email: "rm@tcd.ie", mobile:  "085-2245689", captain: false, password: '123456789'
)
Player.create(
    name:"Victor De Vries", team: 2, email: "vd@tcd.ie", mobile:  "085-3676789", captain: false, password: '123456789'
)
Player.create(
    name:"Jack Spain", team: 2, email: "js@tcd.ie", mobile:  "085-3451189", captain: false, password: '123456789'
)
Player.create(
    name:"Connor Montgomery", team: 2, email: "cm@tcd.ie", mobile:  "085-3456789", captain: false, password: '123456789'
)
Player.create(
    name:"Adam Colton", team: 2, email: "ac@tcd.ie", mobile:  "085-1112233", captain: false, password: '123456789'
)
Player.create(
    name:"Owen Monagan", team: 2, email: "om@tcd.ie", mobile:  "085-3456789", captain: false, password: '123456789'
)
Player.create(
    name:"Louis Murphy", team: 2, email: "lm@tcd.ie", mobile:  "085-7676789", captain: false, password: '123456789'
)
Player.create(
    name:"Ryan Joyce", team: 2, email: "rj@tcd.ie", mobile:  "085-1276789", captain: false, password: '123456789'
)
Player.create(
    name:"Jamie Petch", team: 3, email: "jp@tcd.ie", mobile:  "085-3344789", captain: false, password: '123456789'
)
Player.create(
    name:"Harry Johnson", team: 3, email: "hj@tcd.ie", mobile:  "085-3457777", captain: true, password: '123456789'
)
Player.create(
    name:"Nick Welaratne", team: 3, email: "nw@tcd.ie", mobile: "085-2345678", captain: false, password: '123456789'
)
Player.create(
    name:"Ben Arrowsmith", team: 3, email: "ba@tcd.ie", mobile:  "085-5456789", captain: false, password: '123456789'
)
Player.create(
    name:"Wilf King", team: 3, email: "wk@tcd.ie", mobile:  "085-3400789", captain: false, password: '123456789'
)
Player.create(
    name:"John Drury", team: 3, email: "jd@tcd.ie", mobile:  "085-1156789", captain: false, password: '123456789'
)
Player.create(
    name:"Eoin Buttenshaw", team: 3, email: "eb@tcd.ie", mobile:  "085-3555789", captain: false, password: '123456789'
)
Player.create(
    name:"james Glenn-Craigie", team: 3, email: "jgc@tcd.ie", mobile:  "085-3456789", captain: false, password: '123456789'
)
Player.create(
    name:"Oisin Roche", team: 3, email: "orl@tcd.ie", mobile:  "085-5676789", captain: false, password: '123456789'
)
Player.create(
    name:"Georges Simonin", team: 3, email: "gs@tcd.ie", mobile:  "085-2874789", captain: false, password: '123456789'
)
Player.create(
    name:"Paul Birmingham", team: 3, email: "pb@tcd.ie", mobile:  "085-0986789", captain: false, password: '123456789'
)
Player.create(
    name:"Robin Fitzpatrick", team: 4, email: "rf@tcd.ie", mobile:  "085-3465789", captain: true, password: '123456789'
)
Player.create(
    name:"Ollie Welaratne", team: 4, email: "ow@tcd.ie", mobile:"085-1234567", captain: false, password: '123456789'
)
Player.create(
    name:"Enda Cawley", team: 4, email: "ec@tcd.ie", mobile:  "085-3765489", captain: false, password: '123456789'
)
Player.create(
    name:"Tom Crampton", team: 4, email: "tc@tcd.ie", mobile:  "085-3454329", captain: false, password: '123456789'
)
Player.create(
    name:"Matthew Lewis", team: 4, email: "ml@tcd.ie", mobile:  "085-3456655", captain: false, password: '123456789'
)
Player.create(
    name:"John Browne", team: 4, email: "jb@tcd.ie", mobile:  "085-1234789", captain: false, password: '123456789'
)
Player.create(
    name:"Lorcan Clarke", team: 4, email: "lc@tcd.ie", mobile:  "085-5456555", captain: false, password: '123456789'
)
Player.create(
    name:"Andrew Tyrell", team: 4, email: "at@tcd.ie", mobile:  "085-1444789", captain: false, password: '123456789'
)
Player.create(
    name:"Killian Flood", team: 4, email: "kf@tcd.ie", mobile:  "085-9056789", captain: false, password: '123456789'
)
Player.create(
    name:"Max Bradley", team: 4, email: "mb@tcd.ie", mobile:  "085-7776789", captain: false, password: '123456789'
)
Player.create(
    name:"Max McFarland", team: 4, email: "mm@tcd.ie", mobile:  "085-7111189", captain: false, password: '123456789'
)
Player.create(
    name:"Ollie Kane", team: 4, email: "ok@tcd.ie", mobile:  "085-4446789", captain: false, password: '123456789'
)
