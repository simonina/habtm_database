json.array!(@absences) do |absence|
  json.extract! absence, :id, :reason, :absent_from, :absent_until, :player_id
  json.url absence_url(absence, format: :json)
end
