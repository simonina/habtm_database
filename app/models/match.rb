class Match < ActiveRecord::Base
    has_and_belongs_to_many :players
    
    #def start_time
     #   self.match.time    ##Where 'start' is a attribute of type 'Date' accessible through MyModel's relationship
    #end
end
