class Player < ActiveRecord::Base
  
  has_and_belongs_to_many :matches    #represents has and belongs to many relationship, with a join table involved
  has_many :absences, :dependent => :destroy # player has many absences, depedant means if destoryed absence is destroyed

  devise :database_authenticatable, :registerable,    
         :recoverable, :rememberable, :trackable, :validatable  
    
  def generate_password
    self.password = "12345678"
  end
end
