class ChangeTimeColumnName < ActiveRecord::Migration
  def change
     rename_column :matches, :time, :start_time
  end
end
