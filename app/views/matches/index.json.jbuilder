json.array!(@matches) do |match|
  json.extract! match, :id, :start_time, :team, :against, :competition, :score_for, :score_against
  json.url match_url(match, format: :json)
end
