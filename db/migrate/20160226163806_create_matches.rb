class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.datetime :time
      t.integer :team
      t.string :against
      t.string :competition
      t.integer :score_for
      t.integer :Score_against

      t.timestamps null: false
    end
  end
end
