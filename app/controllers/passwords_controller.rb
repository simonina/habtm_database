class PasswordsController < ApplicationController
  before_action :authenticate_player!
  before_action :ensure_needs_password_change!
  def new
  end

  def create
    
    redirect_to new_password_path if params[:password] != params[:confirmation_password] 
    #redirct to new password path if password != confiimation
    current_player.password = params[:password] #setting current player password to inputted
    current_player.needs_password_change = false # changing flag to false once password is change
    if current_player.save #if saved
      sign_in(current_player, :bypass => true)
      flash[:notice] = "Your password has successfully been changed." #flash message
      redirect_to calendar_index_path # redirect to home page
    else # invalid input
      flash[:error] = "Your password is invalid"
      render :new
    end
  end
  
  private
  
    def ensure_needs_password_change! 
      redirect_to calendar_index_path if !current_player.needs_password_change
    end
end
