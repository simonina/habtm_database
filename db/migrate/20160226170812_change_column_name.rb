class ChangeColumnName < ActiveRecord::Migration
  def change
    rename_column :matches, :Score_against, :score_against
  end
end
