class CreateAbsences < ActiveRecord::Migration
  def change
    create_table :absences do |t|
      t.string :reason
      t.datetime :absent_from
      t.datetime :absent_until
      t.integer :player_id

      t.timestamps null: false
    end
  end
end
