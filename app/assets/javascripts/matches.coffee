# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $('.check_box').change ->
    sum = 0
    $('.check_box').each ->
      if $(this).is(':checked')
        sum++
      return
    $('#players_selected').html sum + ' players selected'
    return
  return