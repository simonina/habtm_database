json.array!(@news) do |news|
  json.extract! news, :id, :start_time, :title, :content
  json.url news_url(news, format: :json)
end
